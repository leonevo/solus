#!/bin/bash
set -e
##################################################################################################################
# Written to be used on 64 bits computers
# Author 	: 	Erik Dubois
# Website 	: 	http://www.erikdubois.be
##################################################################################################################
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

#software from 'normal' repositories

sudo eopkg install -y vivaldi-snapshot
sudo eopkg install -y cantarell-fonts clipit etcher
sudo eopkg install -y font-awesome-ttf font-ubuntu-ttf
sudo eopkg install -y gcolor3 gconf gimp glances gnome-disk-utility
sudo eopkg install -y gparted gradio nautilus-dropbox neofetch noto-sans-ttf peek screenfetch scrot
sudo eopkg install -y simplescreenrecorder source-code-pro telegram vlc vscode xkill 

# hide plank icon

# gsettings set net.launchpad.plank.dock.settings:/net/launchpad/plank/docks/dock1/ show-dock-item false

###############################################################################################

# zip/unzip

sudo eopkg install -y p7zip cabextract

# wiffi

sudo eopkg install -y broadcom-sta-common broadcom-sta-current

echo "################################################################"
echo "###################    core software installed  ################"
echo "################################################################"

