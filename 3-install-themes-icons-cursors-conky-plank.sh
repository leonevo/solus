#!/bin/bash
set -e
##################################################################################################################
# Written to be used on 64 bits computers
# Author 	: 	Erik Dubois
# Adapt to me
# Website 	: 	http://www.erikdubois.be
##################################################################################################################
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

#themes
sudo eopkg install -y vertex-gtk-theme
sudo eopkg install -y arc-gtk-theme arc-icon-theme arc-plank-theme
sudo eopkg install -y numix-icon-theme-circle numix-gtk-theme
sudo eopkg install -y paper-icon-theme paper-gtk-theme
sudo eopkg install -y oranchelo-icon-theme
sudo eopkg install -y evopop-gtk-theme
sudo eopkg install -y papirus-icon-theme

echo "################################################################"
echo "#############       eye candy software         #################"
echo "################################################################"
